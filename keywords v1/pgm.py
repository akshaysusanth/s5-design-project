import io 
import nltk
from nltk.corpus import stopwords 
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
import re, collections
from collections import defaultdict
from PIL import Image
import pytesseract

token_score = 0.0
para_score = 0.0
spell_score = 0.0

def ocr_core(filename):

    text = pytesseract.image_to_string(Image.open(filename))
    
    return text


def sentence_to_wordlist(raw_sentence):
    
    clean_sentence = re.sub("[^a-zA-Z0-9]"," ", raw_sentence)
    tokens = nltk.word_tokenize(clean_sentence)
    return tokens

def tokenize(essay):
    stripped_essay = essay.strip()
    
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    raw_sentences = tokenizer.tokenize(stripped_essay)
    
    tokenized_sentences = []
    for raw_sentence in raw_sentences:
        if len(raw_sentence) > 0:
            tokenized_sentences.append(sentence_to_wordlist(raw_sentence))
    
    return tokenized_sentences


def count_pos(essay):

	tokenized_sentences = tokenize(essay)    
	global noun_count
	noun_count=0
	global adj_count
	adj_count = 0
	global verb_count
	verb_count = 0
	global adv_count
	adv_count = 0
    
	for sentence in tokenized_sentences:
		tagged_tokens = nltk.pos_tag(sentence)
        
		for token_tuple in tagged_tokens:
			pos_tag = token_tuple[1]
            
			if pos_tag.startswith('N'): 
				noun_count += 1
			elif pos_tag.startswith('J'):
				adj_count += 1
			elif pos_tag.startswith('V'):
				verb_count += 1
			elif pos_tag.startswith('R'):
				adv_count += 1
            
	return noun_count, adj_count, verb_count, adv_count

def count_spell_error(essay):
    
	clean_essay = re.sub(r'\W', ' ', str(essay).lower())
	clean_essay = re.sub(r'[0-9]', '', clean_essay)
    
	data = open('words.txt').read()
    
	words_ = re.findall('[a-z]+', data.lower())
    
	word_dict = collections.defaultdict(lambda: 0)
                       
	for word in words_:
		word_dict[word] += 1
                       
	clean_essay = re.sub(r'\W', ' ', str(essay).lower())
	clean_essay = re.sub(r'[0-9]', '', clean_essay)
                        
	global mispell_count
	mispell_count = 0
    
	words = clean_essay.split()
                        
	for word in words:
		if not word in word_dict:
			mispell_count += 1
    
	return mispell_count

def avg_word_len(essay):
    
    clean_essay = re.sub(r'\W', ' ', essay)
    words = nltk.word_tokenize(clean_essay)
    return sum(len(word) for word in words) / len(words)

def stop_words():
	swords = set(stopwords.words('english')) 
	filei = open("essay.txt") 
	lines = filei.read()
	words = lines.split() 
	f = open('stopwords.txt','w') 
	for i in words: 
		if not i in swords: 
			f.write(i)
			f.write(" ")
	f.close()		

def removeDuplicates(duplicate): 
    final_list = [] 
    for string in duplicate: 
        if string not in final_list: 
            final_list.append(string) 
    return final_list 

def scoring():

	tokens = noun_count+adj_count+verb_count+adv_count
	token_score = tokens*0.15
	para_score = (para+1)*10
	if mispell_count<=5:
		spell_score = mispell_count*2
	elif mispell_count>5 and mispell_count<=10:
		spell_score = 15
	elif mispell_count>10:
		spell_score = 20
	return token_score, para_score, spell_score


def compare_essay(text,keywords):
	int count;
	file1 = open('essay.txt', 'r')
	file2 = open('keywords.txt', 'r')

	for line1 in file1:
	    for line2 in file2:
       		 if line1 == line2:
           		 count++


	file1.close()
	file2.close()
	return count

def main():

	text = ocr_core("sample.jpg")
	refText = ocr_core("ref.jpg")

	global para
	para = text.count("\n\n")

	keywords = [word for (word, pos) in nltk.pos_tag(nltk.word_tokenize(refText)) if pos[0] == 'N']
	keywords = removeDuplicates(keywords)
    
	f = open('essay.txt','w') 
	f1 = open('keywords.txt','w') 
	
	for i in text: 
		f.write(i)
		
	for i in keywords: 
		f1.write(i)
		f1.write(" ")
	f.close()
	f1.close()

	
	
	print("The recognized text is: \n\n",text)
    
	print("\n\nKeywords found from reference essay \n", keywords)
	
	print("\n\nNumber of (Nouns, Adjectives, Verbs, Adverbs) = ", count_pos(text))
    
	print("\n\nNumber of spelling mistakes = ", count_spell_error(text))
    
	print("\n\nAverage word length = ", avg_word_len(text))

	print("\n\nNumber of paragraphs = ", para)
    
	stop_words()

	token_score, para_score, spell_score = scoring()
	essay_score = 25.0 + token_score + para_score - spell_score

	print("\n\nEssay score is = ", essay_score,"%")

if __name__ == "__main__": main()
