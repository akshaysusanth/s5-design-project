# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'TEA.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication,QWidget, QVBoxLayout, QPushButton, QFileDialog , QLabel, QTextEdit
import sys
from PyQt5.QtGui import QPixmap
import io 
import nltk
from nltk.corpus import stopwords 
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
import re, collections
from collections import defaultdict
from PIL import Image
import pytesseract
from grammarbot import GrammarBotClient

client = GrammarBotClient(api_key='KS9C5N3Y')

token_score = 0.0
para_score = 0.0
neg_score = 0.0
ref_score = 0.0
grammar_score = 0.0
imgLoc=[]

class Ui_MainWindow(QWidget):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("Essay Assessor App")
        MainWindow.resize(1280, 720)
        MainWindow.setAcceptDrops(True)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(-210, -30, 1891, 961))
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.RichText)
        self.label.setPixmap(QtGui.QPixmap("yolo.jpg"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(447, 20, 386, 32))
        self.label_2.setTextFormat(QtCore.Qt.RichText)
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(870, 580, 80, 25))
        self.pushButton.setObjectName("pushButton")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 120, 621, 441))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_2.setGeometry(QtCore.QRect(650, 120, 651, 441))
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(760, 650, 80, 25))
        self.pushButton_3.setObjectName("pushButton_3")
        self.plainTextEdit_3 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_3.setGeometry(QtCore.QRect(540, 630, 201, 61))
        self.plainTextEdit_3.setObjectName("plainTextEdit_3")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(350, 580, 80, 25))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(190, 580, 80, 25))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(1030, 580, 80, 25))
        self.pushButton_5.setObjectName("pushButton_5")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(270, 90, 121, 22))
        self.label_3.setTextFormat(QtCore.Qt.RichText)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(900, 90, 172, 22))
        self.label_4.setObjectName("label_4")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.pushButton.clicked.connect(self.getImage2)
        self.pushButton_4.clicked.connect(self.enter1)
        self.pushButton_2.clicked.connect(self.getImage1)
        self.pushButton_3.clicked.connect(self.finalPgm)
        self.pushButton_5.clicked.connect(self.enter2)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("Essay Assessor App", "Essay Assessor App"))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:20pt; font-weight:600; color:#ffffff;\">TECHNICAL ESSAY ASSESSOR</span></p></body></html>"))
        self.pushButton.setText(_translate("MainWindow", "Browse"))
        self.pushButton_3.setText(_translate("MainWindow", "Grade"))
        self.pushButton_4.setText(_translate("MainWindow", "Enter"))
        self.pushButton_2.setText(_translate("MainWindow", "Browse"))
        self.pushButton_5.setText(_translate("MainWindow", "Enter"))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; font-weight:600; color:#ffffff;\">INPUT ESSAY</span></p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:14pt; font-weight:600; color:#ffffff;\">REFERENCE ESSAY</span></p></body></html>"))




    def getImage1(self):
        self.fname = QFileDialog.getOpenFileName(self, 'Open file',
                                            'home', "Image files (*.jpg *.png)")
        imgLoc.append(self.fname[0])
        line=self.fname[0]+"\n\nImage loaded.\nClick Enter to Recognize text."
        self.plainTextEdit.insertPlainText(line)
        
    def getImage2(self):
        self.fname = QFileDialog.getOpenFileName(self, 'Open file',
                                            'home', "Image files (*.jpg *.png)")
        line=self.fname[0]+"\n\nImage loaded.\nClick Enter to Recognize text."
        self.plainTextEdit_2.insertPlainText(line) 
        imgLoc.append(self.fname[0])       
        
        
    def enter1(self):
        self.plainTextEdit.clear()
        line=ocr_core(self.fname[0])
        self.plainTextEdit.insertPlainText(line) 
        
    def enter2(self):
        self.plainTextEdit_2.clear()
        line=ocr_core(self.fname[0])
        self.plainTextEdit_2.insertPlainText(line)          

    def finalPgm(self):
        score=driverPgm()
        line="\n"+str(score)
        self.plainTextEdit_3.insertPlainText(str(line))
      
       
def ocr_core(filename):

	img=Image.open(filename)
	text = pytesseract.image_to_string(img)
	return text

def sentence_to_wordlist(raw_sentence):
    
    clean_sentence = re.sub("[^a-zA-Z0-9]"," ", raw_sentence)
    tokens = nltk.word_tokenize(clean_sentence)
    return tokens

def tokenize(essay):
    stripped_essay = essay.strip()
    
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    raw_sentences = tokenizer.tokenize(stripped_essay)
    
    tokenized_sentences = []
    for raw_sentence in raw_sentences:
        if len(raw_sentence) > 0:
            tokenized_sentences.append(sentence_to_wordlist(raw_sentence))
    
    return tokenized_sentences

def count_pos(essay):

	tokenized_sentences = tokenize(essay)    
	global noun_count
	noun_count=0
	global adj_count
	adj_count = 0
	global verb_count
	verb_count = 0
	global adv_count
	adv_count = 0
    
	for sentence in tokenized_sentences:
		tagged_tokens = nltk.pos_tag(sentence)
        
		for token_tuple in tagged_tokens:
			pos_tag = token_tuple[1]
            
			if pos_tag.startswith('N'): 
				noun_count += 1
			elif pos_tag.startswith('J'):
				adj_count += 1
			elif pos_tag.startswith('V'):
				verb_count += 1
			elif pos_tag.startswith('R'):
				adv_count += 1
            
	return noun_count, adj_count, verb_count, adv_count


def avg_word_len(essay):
    
    clean_essay = re.sub(r'\W', ' ', essay)
    words = nltk.word_tokenize(clean_essay)
    return sum(len(word) for word in words) / len(words)

def stop_words():
	swords = set(stopwords.words('english')) 
	filei = open("essay.txt") 
	lines = filei.read()
	words = lines.split() 
	f=[]
	for i in words: 
		if not i in swords: 
			f.append(i)
	return f
	
def removeDuplicates(duplicate): 
    final_list = [] 
    for string in duplicate: 
        if string not in final_list: 
            final_list.append(string) 
    return final_list 

def scoring():

	tokens = noun_count+adj_count+verb_count+adv_count
	token_score = tokens*0.15

	if((para+1)==1):
		para_score = -20
	elif((para+1)==2):
		para_score = -10
	elif((para+1)<(int(word_count/150))):
		para_score = -10
	elif((para+1)>(int(word_count/150))):
		para_score = -5
	elif((para+1)==(int(word_count/150))):
		para_score = 15

	print("Para score is - ",para_score)

	if neg_count<=5:
		neg_score = neg_count*4
	elif neg_count>5 and neg_count<=10:
		neg_score = 20
	elif neg_count>10:
		neg_score = 30
	return token_score, para_score, neg_score

def neg_check(text):
	res = client.check(text)
	global match
	match = res.matches
	global neg_count
	neg_count = len(match)

def compare_essay(swords,keywords):
	count=0
	for line1 in keywords:
		for line2 in swords:
			if line1 == line2:
				count = count + 1
	ref_score = count * 1.5
	return ref_score

def ktu(m):
	if m>90:
		grade='O'
	elif m>84 and m<91:
		grade='A+'
	elif m>80 and m<85:
		grade='A'
	elif m>70 and m<81:
		grade='B+'
	elif m>60 and m<71:
		grade='B'
	elif m>50 and m<61:
		grade='C'
	elif m>44 and m<51:
		grade='P'
	elif m<45:
		grade='F'

	return grade


def driverPgm():
	
	text = ocr_core(imgLoc[0])
	refText = ocr_core(imgLoc[1])
	essay_score = 0

	global word_count
	word_count = len(text.split())

	neg_check(text)

	global para
	para = text.count("\n\n")
	keywords = [word for (word, pos) in nltk.pos_tag(nltk.word_tokenize(refText)) if pos[0] == 'N']
	keywords = removeDuplicates(keywords)
	swords = stop_words()
	rscore = compare_essay(swords,keywords)
    
	f = open('essay.txt','w') 
	f1 = open('keywords.txt','w') 
	
	for i in text: 
		f.write(i)
		
	for i in keywords: 
		f1.write(i)
		f1.write(" ")
	
	list1 = word_tokenize(text)  
	list2 = word_tokenize(refText) 
   
	sw = stopwords.words('english')  
	l1 =[]
	l2 =[] 
  
	X = {w for w in list1 if not w in sw}  
	Y = {w for w in list2 if not w in sw} 
	vector = X.union(Y)  
	for w in vector: 
		if w in X: l1.append(1) 
		else: l1.append(0) 
		if w in Y: l2.append(1) 
		else: l2.append(0)
     
	c = 0
 
	for i in range(len(vector)): 
		c+= l1[i]*l2[i] 
	sim = c / float((sum(l1)*sum(l2))**0.5)
	sim_score = sim*50

	print("\nCosine Similarity Score: ", sim*100,"%")
	
	f.close()
	f1.close()

	print("The recognized text is: \n\n",text)
	print("\n\nKeywords found from reference essay \n", keywords)
	print("\n\nNumber of (Nouns, Adjectives, Verbs, Adverbs) = ", count_pos(text))
	print("\n\nNumber of spelling and grammar mistakes = ", len(match))
	print("\n\nAverage word length = ", avg_word_len(text))
	print("\n\nNumber of paragraphs = ", para+1)
	print("\n\nNumber of words = ", word_count)
	
	ps = PorterStemmer() 
	words=[]
	for w in swords: 
		words.append(ps.stem(w))
	print(words)
	
	token_score, para_score, neg_score = scoring()
	
	essay_score = 10 + rscore + token_score + para_score - neg_score + sim_score
	grade = ktu(essay_score)
	
	print("\n\nEssay score is = ", essay_score,"%")
	print("\n\nEssay grade is = ", grade)
	if(essay_score>100):
		return 100
	elif(essay_score<0):
		return 0
	else:
		return essay_score


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

