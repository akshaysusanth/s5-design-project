from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
from sklearn.metrics.pairwise import cosine_similarity
  
f1 = open("write.txt") 
f2 = open("one.txt") 

F1 =f1.read()
F2 =f2.read()
  
list1 = word_tokenize(F1)  
list2 = word_tokenize(F2) 
   
sw = stopwords.words('english')  
l1 =[]
l2 =[] 
  
X = {w for w in list1 if not w in sw}  
Y = {w for w in list2 if not w in sw} 

vector = X.union(Y)  
for w in vector: 
    if w in X: l1.append(1) 
    else: l1.append(0) 
    if w in Y: l2.append(1) 
    else: l2.append(0)
     
c = 0
print("\nEssay1:",F1)
print("\nEssay2:",F2)
 
for i in range(len(vector)): 
        c+= l1[i]*l2[i] 
sim = c / float((sum(l1)*sum(l2))**0.5) 

print("Similarity: ", sim*100,"%\n")
