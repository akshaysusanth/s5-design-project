import cv2
image = cv2.imread('img.jpg')

blurred = cv2.pyrMeanShiftFiltering (image, 31,91)

gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)

ret , threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
edged = cv2.Canny(gray, 30, 200) 
contours, hierarchy = cv2.findContours(edged,  
    cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) 
cv2.drawContours(image, contours, -1, (0, 0, 255), 3)
cv2.namedWindow('Display',cv2.WINDOW_NORMAL)
cv2.imshow('Display',image)
cv2.waitKey()
