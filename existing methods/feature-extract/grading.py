import re, collections

def word_count():
    return len(essay.split())

def sentence_count():
    sentences = 0
    for line in essay.split():
        sentences += line.count('.') + line.count('!') + line.count('?') 
    return sentences

def count_spell_error(essay):    
	clean_essay = re.sub(r'\W', ' ', str(essay).lower())
	clean_essay = re.sub(r'[0-9]', '', clean_essay)
	data = open('words.txt').read()    
	words_ = re.findall('[a-z]+', data.lower())    
	word_dict = collections.defaultdict(lambda: 0)                       
	for word in words_:
		word_dict[word] += 1                       
	clean_essay = re.sub(r'\W', ' ', str(essay).lower())
	clean_essay = re.sub(r'[0-9]', '', clean_essay)                        
	global mispell_count
	mispell_count = 0    
	words = clean_essay.split()                        
	for word in words:
		if not word in word_dict:
			mispell_count += 1    
	return mispell_count

f = open("essay.txt", "r")
essay = f.read()
wc = word_count()
sc = sentence_count()
spell_error = count_spell_error(essay)
score = (wc/10)+sc-spell_error
print("The word count is: ", wc)
print("The sentence count is: ", sc)
print("The spelling error count is: ", spell_error)
print("Essay score is: ", score,"%")
f.close()