
def checkSentence(string): 


	length = len(string) 

	
	if string[0] < 'A' or string[0] > 'Z': 
		return False

	
	if string[length-1] != '.': 
		return False

	
	prev_state = 0
	curr_state = 0

	
	index = 1

	
	while (string[index]): 
		
		if string[index] >= 'A' and string[index] <= 'Z': 
			curr_state = 0

		
		elif string[index] == ' ': 
			curr_state = 1

		
		elif string[index] >= 'a' and string[index] <= 'z': 
			curr_state = 2

		
		elif string[index] == '.': 
			curr_state = 3

		
		if prev_state == curr_state and curr_state != 2: 
			return False

		
		if prev_state == 2 and curr_state == 0: 
			return False

		
		if curr_state == 3 and prev_state != 1: 
			return True

		index += 1

		prev_state = curr_state 

	return False


//insert the essay here

string = ["I love cinema.", "The vertex is S.", 
			"I am single.", "My name is KG.", 
			"I lovE cinema.", "good Morning. welcome to todays news.", 
			"I love superheroes and Comicbooks.", 
			" You are my friend.", "I love cinema"] 
string_size = len(string) 
for i in xrange(string_size): 
	if checkSentence(string[i]): 
		print "\"" + string[i] + "\" is correct"
	else: 
		print "\"" + string[i] + "\" is incorrect"



